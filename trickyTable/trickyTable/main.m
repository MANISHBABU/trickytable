//
//  main.m
//  trickyTable
//
//  Created by Manish on 09/01/14.
//  Copyright (c) 2014 Manish@esense. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
