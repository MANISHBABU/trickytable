//
//  TrickyTable.h
//  trickyTable
//
//  Created by Manish on 09/01/14.
//  Copyright (c) 2014 Manish@esense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrickyTable : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UITableView *trickyTable;
    NSMutableArray *arrayDataForTrickyTable;
    
    NSMutableIndexSet *expandedSections;
    NSMutableDictionary *selectedIndexes;
    
    
}
-(void)gesture;
-(void)handleSwipeDown;
-(void)handleSwipeLeft;
-(void)handleSwipeRight;

@end
