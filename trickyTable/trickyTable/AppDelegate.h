//
//  AppDelegate.h
//  trickyTable
//
//  Created by Manish on 09/01/14.
//  Copyright (c) 2014 Manish@esense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
