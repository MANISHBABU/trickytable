//
//  TrickyTable.m
//  trickyTable
//  Help li hai http://www.thinkandbuild.it/page/3/ iss site se
//  Created by Manish on 09/01/14.
//  Copyright (c) 2014 Manish@esense. All rights reserved.
//

#import "TrickyTable.h"
#import <QuartzCore/QuartzCore.h>


@interface TrickyTable ()

@end

@implementation TrickyTable
#define kCellHeight 50.0
static NSString *cellIdentifier;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidUnload {
    selectedIndexes = nil;
	
	[super viewDidUnload];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    selectedIndexes = [[NSMutableDictionary alloc] init];
    
    [self gesture];
    arrayDataForTrickyTable = [[NSMutableArray alloc]initWithObjects:@"Manish Kumar Pathak",@"Sudeep Jaiswal",@"Abhijit ;)", nil];
    cellIdentifier = @"rowCell";
    [trickyTable registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    
    trickyTable.layer.cornerRadius =30.0f;
    trickyTable. sectionIndexColor= [UIColor redColor];
    
    
    
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Populating Table View

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return  3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [arrayDataForTrickyTable count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.text = [arrayDataForTrickyTable objectAtIndex:indexPath.row];
    
    //This function is where all the magic happens
    
    [cell.layer setCornerRadius:7.0f];
    [cell.layer setMasksToBounds:YES];
    [cell.layer setBorderWidth:2.0f];
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor orangeColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.9];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    
    cell.backgroundView = [[UIView alloc] initWithFrame: CGRectZero];
    [cell.backgroundView setNeedsDisplay];
    [trickyTable setBackgroundColor:[UIColor lightGrayColor]];
    
    return cell;
    [UIView commitAnimations];
    
}


// Giving Title To Each Sections

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    
    if(section == 0)
    {
        return (@"Abhijit");
        
    }
    else if(section == 1)
    {
        return (@"Sudeep");
        
    }
    else
    {
        return (@"Manish");
        
    }
    
}


//Gesture To UITableView
-(void)gesture{
    
    //Add a left swipe gesture recognizer
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft)];
    recognizer.delegate = self;
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [trickyTable addGestureRecognizer:recognizer];
    
    //Add a Down swipe gesture recognizer
    UISwipeGestureRecognizer *recognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown)];
    recognizer2.delegate = self;
    [recognizer2 setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [trickyTable addGestureRecognizer:recognizer2];
    
    //Add a right swipe gesture recognizer
    UISwipeGestureRecognizer *recognizer3 = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(handleSwipeRight)];
    recognizer3.delegate = self;
    [recognizer3 setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [trickyTable addGestureRecognizer:recognizer3];
    
}

-(void)handleSwipeDown{
    
    NSLog(@"Down");
}

-(void)handleSwipeRight{
    
    NSLog(@"Right Swipe");
}


-(void)handleSwipeLeft{
    
    [trickyTable beginUpdates];
    NSLog(@"Left Swipe");
    [trickyTable deleteSections:expandedSections withRowAnimation:UITableViewRowAnimationFade];
    [trickyTable endUpdates];
    NSLog(@"%@",expandedSections);
}


- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section>0) return YES;
    
    return NO;
}


//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    UIView *headerView =[[UIView alloc]initWithFrame:CGRectMake(trickyTable.frame.origin.x, trickyTable.frame.origin.y, trickyTable.frame.size.width, trickyTable.frame.size.height)];
//  //  UIView *headerView = [[UIView alloc] initWithFrame:[CGRectMake(0, 0, trickyTable.frame.size.width, trickyTable.frame.size.width)];
//    if (section == 2)
//        [headerView setBackgroundColor:[UIColor redColor]];
//    else
//        [headerView setBackgroundColor:[UIColor clearColor]];
//    headerView.layer.cornerRadius =30.0f;
//
//    return headerView;
//}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // modelForSection is a custom model object that holds items for this section.
        // [expandedSections removeItem:[self itemForRowAtIndexPath:indexPath]];
        
        [tableView beginUpdates];
        
        // Section is now completely empty, so delete the entire section.
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                 withRowAnimation:UITableViewRowAnimationFade];
        
        
        [tableView endUpdates];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%ld row selected of section %ld",(long)indexPath.row,(long)indexPath.section);
    
    // Deselect cell
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    // Toggle 'selected' state
    BOOL isSelected = ![self cellIsSelected:indexPath];
    
    // Store cell 'selected' state keyed on indexPath
    NSNumber *selectedIndex = [NSNumber numberWithBool:isSelected];
    [selectedIndexes setObject:selectedIndex forKey:indexPath];
    
    // This is where magic happens...
    [trickyTable beginUpdates];
    [trickyTable endUpdates];
    
}



- (BOOL)cellIsSelected:(NSIndexPath *)indexPath {
	// Return whether the cell at the specified index path is selected or not
	NSNumber *selectedIndex = [selectedIndexes objectForKey:indexPath];
	return selectedIndex == nil ? FALSE : [selectedIndex boolValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // If our cell is selected, return double height
    if([self cellIsSelected:indexPath]) {
        return kCellHeight * 2.0;
    }
    
    // Cell isn't selected so return single height
    return kCellHeight;
}

@end
